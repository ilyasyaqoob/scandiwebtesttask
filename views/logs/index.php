<h1 class="heading_title">My Time Logs</h1>
<div class="container">
  <div class="float-right sidebar">
  <strong>Add New Time Log</strong>
    <div class="add_form">
      <form class="addLog" action="javascript:void(0)" method="post">
        <div class="input-box">
          <label for="description">Description: </label>
          <input class="input" name="description" value="" required/>
        </div>
        <div class="input-box">
          <label for="timespent">Time spent: </label>
          <input class="input" name="timespent" value="" required/>
        </div>
        <input type="submit" value="Add" />
      </form>
    </div>
  </div>
  <div class="fload-left content">
    <div id="timeLogs">
      <?php 
			echo $this->aLogs; 
        ?>
    </div>
  </div>
</div>
