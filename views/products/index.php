<h1 class="page_title float-left">Product List</h1>

<form id="deleteProd"  method="post" action="<?php echo URL;?>products/delete">
<div class="float-right">
    <label>Action</label><select name="delete_products"><option value="">Select</option><option value="mass-delete">Delete Selected</option></select><input type="submit" value="Apply" />
   <a href="<?php echo URL;?>products/add" class="btn">Add New Product</a> 
</div>

<div class="clear"></div>
<?php if(!empty($this->msg)) echo '<p class="message '.$this->msgtype.'">'.$this->msg.'</p>' ?>
<hr />

<div id="all_products">
 <?php 
			echo $this->aProducts; 
        ?>
</div>
</form>