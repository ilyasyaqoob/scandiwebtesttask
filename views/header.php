<!DOCTYPE html>
<html>
<head>
    <title><?php echo (isset($this->title)) ? $this->title : 'Scandiweb Test Task'; ?></title>
    
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css" />    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo URL; ?>public/js/custom.js"></script>
    <?php 
    if (isset($this->js)) 
    {
        foreach ($this->js as $js)
        {
            echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
        }
    }
    ?>
    <script type="text/javascript">
    var ajaxurl = "<?php echo URL?>";
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>


    
<div id="header">

        <a href="<?php echo URL; ?>">Home</a>
        <a href="<?php echo URL; ?>logs">Time Logs</a>
        <a href="<?php echo URL; ?>products">Products</a>

</div>
    
<div id="content">
    
    