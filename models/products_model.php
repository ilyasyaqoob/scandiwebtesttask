<?php

class Products_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

   public function productsList($page = 1)
    {
		$htmlArray = array();
		$limit = 12;
		$links = 7;
		
		$query = "SELECT * FROM products ORDER By date Desc";
		
		$Paginator  = new Paginator($query );
		$data    =  $Paginator->getData( $page, $limit );

		if(!empty($data->data)){			
			$htmlArray[] = '<div class="row">';
			foreach($data->data as $prod){
				$type_value = unserialize($prod['type_value']);
				$htmlArray[] = '<div class="column">';
					$htmlArray[] = '<input class="checkbox" type="checkbox" name="checkAll[]" value="'.$prod['id'].'">';
					$htmlArray[] = '<div class="sku">'.$prod['sku'].'</div>';
					$htmlArray[] = '<div class="name">'.$prod['name'].'</div>';
					$htmlArray[] = '<div class="price">'.CURRENCY.$prod['price'].'</div>';
					if($prod['type'] == 'Book'){
						$htmlArray[] = '<div class="type">'.$type_value['label'] . ': '. $type_value['weight'].' KG</div>';
					}else if($prod['type'] == 'DVD'){
						$htmlArray[] = '<div class="type">'.$type_value['label'] . ': '. $type_value['size'].' MB</div>';	
					}else if($prod['type'] == 'Furniture'){
						$htmlArray[] = '<div class="type">'.$type_value['label'] . ': '. $type_value['height']. 'x' .$type_value['width'] . 'x' .$type_value['length'].'</div>';		
					}
				$htmlArray[] = '</div>';
			}
			$htmlArray[] = '</div>';
        }else{
            $htmlArray[] = '<div class="row"><div class="no-data-found">No product found!</div></div>';	
        }
		
		
		if($data->total > $limit)
			$htmlArray[] = $Paginator->createLinks( $links, 'pagination pagination-sm' );
		
		return implode('', $htmlArray);
    }
    
    public function getProduct($sku)
    {
        return $this->db->select('SELECT * FROM products WHERE sku = :sku', array(':sku' => $sku));
    }
    
    public function create($data)
    {
		//print_r($data);
		$isExist  = $this->getProduct($data['sku']);
		//print_r($isExist);
		if(empty($isExist)){
			$this->db->insert('products', array(
				'name' => $data['name'],
				'sku' => $data['sku'],
				'type' => $data['type'],
				'type_value' => $data['type_value'],
				'price' => $data['price'],
				'date' => $data['date']
			));
			$msg['type'] = 'success';
			$msg['msg'] = 'Added Successfully';
			
		}else{
			$msg['type'] = 'error';
			$msg['msg'] = 'Sku already exists. Please type correct sku!';
			
		}
		return $msg;	
    }
    
    
    public function delete($data)
    {
		foreach($data['checkAll'] as $item){
        	$this->db->delete('products', 'id='.$item);
		}
		
		$msg['type'] = 'success';
		$msg['msg'] = 'Deleted Successfully';
		
		return $msg;
		
       
    }
}