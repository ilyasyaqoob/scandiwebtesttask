<?php

class Logs_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function logsList($page = 1)
    {
		$htmlArray = array();
		$limit = 10;
		$links = 8;
		
		$query = "SELECT * FROM logs GROUP BY date ORDER By date Desc";
		
		$Paginator  = new Paginator($query );
		$data    =  $Paginator->getData( $page, $limit );
		//print_r($data);
		if(!empty($data->data)){			
			$datenow = date('d/m/Y');
			$previous = date('2000-01-01');
			$headings = '<div class="row row-header"><div class="column">Description</div><div class="column">Time Spent</div><div class="column">Date</div></div>';
			foreach($data->data as $log){
				$current = date('d/m/Y', strtotime($log['date']));
				if($current == $datenow && $current != $previous ){
					$htmlArray[] =  '<div class="headding">Today</div>';
					$htmlArray[] = $headings;
					$previous = $current;
				}else if($current != $previous ){
					$htmlArray[] = '<div class="headding">'.date('d.m.Y', strtotime($log['date'])).'</div>';
					$htmlArray[] = $headings;
					$previous = $current;
				}
            	$htmlArray[] = '<div class="row">';
					$htmlArray[] = '<div class="column">'.$log['description'].'</div>';
					$htmlArray[] = '<div class="column">'.$log['timespent'].'</div>';
					$htmlArray[] = '<div class="column">'.date('d.m.Y h:i A', strtotime($log['date'])).'</div>';
				$htmlArray[] = '</div>';
			}
			
			
        }else{
            $htmlArray[] = '<div class="row"><div class="no-data-found">No log found!</div></div>';	
        }
		
		if($data->total > $limit)
			$htmlArray[] = $Paginator->createLinks( $links, 'pagination pagination-sm' );
		
		
		
		
		return implode('', $htmlArray);
    }
    
       
    public function create($data)
    {
        $this->db->insert('logs', array(
            'description' => $data['description'],
            'timespent' => $data['timespent'],
            'date' => $data['date']
        ));
    }
    
    
}