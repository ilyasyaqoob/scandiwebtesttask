$(document).ready(function() {
    jQuery(document).on('submit','form.addLog',function(){
		var formData = jQuery(this).serialize();
		
		jQuery.ajax({
			type:"POST",
			url: ajaxurl+'logs/create',
			data: formData,
			success:function(responseData){
				//console.log('add Successfully');
				$('form.addLog')[0].reset();
				loadData(1, ajaxurl+'logs/showlist');
			}
					
		});
		
		
		return false;
	});
	
	jQuery(document).on('change','#addProduct select[name="type"]',function(){
		var seledtedValue = $(this).val();
		var typeSwitcher = $('.type_switcher');
		if(seledtedValue == ''){
			typeSwitcher.html('');	
		}else if(seledtedValue == 'Book'){
			typeSwitcher.html('');
			typeSwitcher.append(createInput('Weight', 'weight', 'floatOnly', 'Please provide weight in KG'));
		}else if(seledtedValue == 'DVD'){
			typeSwitcher.html('');
			typeSwitcher.append(createInput('Size', 'size', 'floatOnly','Please provide Size in MB\'s'));
		}else if(seledtedValue == 'Furniture'){
			typeSwitcher.html('');
			typeSwitcher.append(createInput('Height', 'height','numbersOnly', ''));
			typeSwitcher.append(createInput('Width', 'width', 'numbersOnly', ''));
			typeSwitcher.append(createInput('Length', 'length', 'numbersOnly', 'Please provide dimensions in HxWxL format'));			
			
		}
		
		return false;
	});
	
	jQuery(document).on('click','#timeLogs .pagination a',function(){
		var parent = $(this).parent();
		if(!parent.hasClass('disabled')){
		var pageNymber = parent.data('page');
		loadData(pageNymber, ajaxurl+'logs/showlist');	
		}
		return false;
	});
	
	jQuery(document).on('click','#all_products .pagination a',function(){
		var parent = $(this).parent();
		if(!parent.hasClass('disabled')){
		var pageNymber = parent.data('page');
		loadData(pageNymber, ajaxurl+'products/showlist');	
		}
		return false;
	});
	
	function loadData(pageNymber, path){
		jQuery.ajax({
			type:"POST",
			data: 'page='+pageNymber,
			url: path,
			success:function(responseData){
				//console.log(responseData);
				$('#timeLogs').html(responseData);
			}
					
		});
	}
	
	function createInput(label, name, inputclass, desc){
		return '<label for="'+label+'">'+label+'</label><input class="'+inputclass+'" id="'+label+'" type="text" name="'+name+'" required="required" /><br /><p class="desc">'+desc+'</p>';
	}
	
	jQuery(document).on('submit','#deleteProd',function(){
		if ( !$('input.checkbox', this).prop('checked')) {
			alert('Please check product first');
			return false;
		}
	});
	
	jQuery(document).on('keypress','.numbersOnly',function(e){
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         e.preventDefault();
    	}
	});
	
	jQuery(document).on('keypress','.floatOnly',function(){
	  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
		event.preventDefault();
	  }
	});
    
});