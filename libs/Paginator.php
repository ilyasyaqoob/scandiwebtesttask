<?php
 
class Paginator {
 
	private $_limit;
	private $_page;
	private $_query;
	private $_total;
		
	public function __construct( $query ) {
		 
		$this->_query = $query;
		
		$this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
		
		///$db = new Database();
	 
		$rs= $this->db->select( $this->_query );
		$this->_total = count($rs);
		 
	}
			
	public function getData( $page = 1, $limit = 10  ) {
		 
		$this->_limit   = $limit;
		$this->_page    = $page;
		$results = array();
	 
		if ( $this->_limit == 'all' ) {
			$query      = $this->_query;
		} else {
			$query      = $this->_query . " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";
		}
		$rs             = $this->db->select( $query );
	 
		foreach($rs as $row ) {
			$results[]  = $row;
		}
	 
		$result         = new stdClass();
		$result->page   = $this->_page;
		$result->limit  = $this->_limit;
		$result->total  = $this->_total;
		$result->data   = ($results)? $results : '';
	 
		return $result;
	} 
	
	public function createLinks( $links, $list_class ) {
		if ( $this->_limit == 'all' ) {
			return '';
		}
	 
		$last       = ceil( $this->_total / $this->_limit );
	 
		$start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
		$end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
	 
		$html       = '<ul class="' . $list_class . '">';
	 
		$class      = ( $this->_page == 1 ) ? "disabled" : "";
		$html       .= '<li data-page="'.( $this->_page - 1 ).'"  class="' . $class . '"><a href="?limit=' . $this->_limit . '&page=' . ( $this->_page - 1 ) . '">&laquo;</a></li>';
	 
		if ( $start > 1 ) {
			$html   .= '<li data-page="'. $this->_limit.'" ><a href="?limit=' . $this->_limit . '&page=1">1</a></li>';
			$html   .= '<li class="disabled"><span>...</span></li>';
		}
	 
		for ( $i = $start ; $i <= $end; $i++ ) {
			$class  = ( $this->_page == $i ) ? "active" : "";
			$html   .= '<li data-page="'.$i.'" class="' . $class . ' ' . $i . '"><a href="?limit=' . $this->_limit . '&page=' . $i . '">' . $i . '</a></li>';
		}
	 
		if ( $end < $last ) {
			$html   .= '<li class="disabled"><span>...</span></li>';
			$html   .= '<li data-page="'.$last.'"  ><a href="?limit=' . $this->_limit . '&page=' . $last . '">' . $last . '</a></li>';
		}
	 
		$class      = ( $this->_page == $last ) ? "disabled" : "";
		$html       .= '<li data-page="'.( $this->_page + 1 ).'"   class="' . $class . '"><a href="?limit=' . $this->_limit . '&page=' . ( $this->_page + 1 ) . '">&raquo;</a></li>';
	 
		$html       .= '</ul>';
	 
		return $html;
	}
}