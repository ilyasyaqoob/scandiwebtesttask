<?php

class Logs extends Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() 
    {    
        $this->view->title = 'Logs';
        $this->view->aLogs = $this->model->logsList();
        
        $this->view->render('header');
        $this->view->render('logs/index');
        $this->view->render('footer');
    }
	
	public function showlist() 
    {
		$page = 1;
		if($_POST['page']) $page = $_POST['page'];
        $this->view->aLogs = $this->model->logsList($page);
		echo $this->view->aLogs;
    }
    
    public function create() 
    {
        $data = array();
        $data['description'] = $_POST['description'];
        $data['timespent'] = $_POST['timespent'];
        $data['date'] = date("Y-m-d h:i:s");
        
        // @TODO: Do your error checking!
       // print_r($data);
        $this->model->create($data);
        header('location: ' . URL . 'logs');
    }
    
   
}