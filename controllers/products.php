<?php

class Products extends Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() 
    {    
        $this->view->title = 'Products';
		$this->view->msg = '';
		$this->view->msgtype = '';
		if(isset($_GET['msg'])){
			$this->view->msg = $_GET['msg'];
			$this->view->msgtype = $_GET['msgtype'];
		}
        $this->view->aProducts = $this->model->productsList();
        
        $this->view->render('header');
        $this->view->render('products/index');
        $this->view->render('footer');
    }
    
    public function create() 
    {
        $data = array();
		$type_value = array();
        $data['name'] = $_POST['name'];
        $data['sku'] = $_POST['sku'];
        $data['type'] = $_POST['type'];
		if($_POST['type'] == 'Book'){
			$type_value['label'] = 'Weight';
			$type_value['weight'] = $_POST['weight'];
		}else if($_POST['type'] == 'DVD'){
			$type_value['label'] = 'Size';
			$type_value['size'] = $_POST['size'];
		}else if($_POST['type'] == 'Furniture'){
			$type_value['label'] = 'Dimension';
			$type_value['height'] = $_POST['height'];
			$type_value['width'] = $_POST['width'];
			$type_value['length'] = $_POST['length'];	
		}
		$data['type_value'] = serialize($type_value);
		$data['price'] = $_POST['price'];
		$data['date'] = date("Y-m-d h:i:s");
        
        // @TODO: Do your error checking!
        //print_r($data);
		
        $msg = $this->model->create($data);
        header('location: ' . URL . 'products?msgtype='.$msg['type'].'&msg='.$msg['msg']);
    }
    
    public function add() 
    {
        $this->view->title = 'Add New Product';
        
        $this->view->render('header');
        $this->view->render('products/add');
        $this->view->render('footer');
    }
    
      
    public function delete()
    {
		
        $msg = $this->model->delete($_POST);
        header('location: ' . URL . 'products?msgtype='.$msg['type'].'&msg='.$msg['msg']);
    }
}